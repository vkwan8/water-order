// Water_order.cpp : This file contains the 'main' function. Program execution begins and ends there.
/* Oriental tetrahedral order parameter calculator
 Written by Shoubhik Raj Maiti, Aug 2021 */

/* Reading of chemical structure files is done with Chemfiles by Guillaume Fraux
 Command line argument parsing is done with TCLAP by Mike Smoot and Daniel Aarno */

// 

#include <chemfiles.hpp>
#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <array>
#include <tclap/CmdLine.h>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <omp.h>
#include <cfenv>
#include <voro++.hh>

// when using MSVC++, include basetsd.h for SSIZE_T because it's ancient OpenMP 2.0
// does not support unsigned index for OpenMP for loops. Intel C++ also defines
//  _MSC_VER on Windows but they support OpenMP > 4.0 so check for them explicitly.
// In those compilers, there is no need to use unsigned index!!
#if defined(_MSC_VER) && !defined(__INTEL_COMPILER) && !defined(__INTEL_LLVM_COMPILER)
 #include <basetsd.h>
 typedef SSIZE_T openmp_index_type;
#else
 typedef size_t openmp_index_type;
#endif

using std::cout;
using std::string;
using std::cin;

bool file_exists(const string& str);
std::array<double, 3> get_ord_parm(const chemfiles::Frame& inputframe, const std::vector<double>& dist_list, size_t dist_list_size, size_t center, const std::vector<size_t>& oxy_ind_list);
bool is_within_bounds(const double value, const double low, const double high);
bool is_cell_at_edge(const std::vector<int>& neighbour_list);
chemfiles::Vector3D get_center_of_mass(const chemfiles::Frame& inputframe, const std::vector<double>& mass_list,const size_t natoms,const double mass_total, const bool no_com_arg);

constexpr double one_over_three = 1.0 / 3.0;
constexpr double three_over_eight = 3.0 / 8.0;
constexpr double num_to_dens = (18.01528 * 1.66054);
constexpr double four_pi_over_three = 3.141592653589793 * 4.0 / 3.0;

// Wrap everything into a try, except block to handle exceptions

int main(int argc, char** argv)
{
    // ready cin for throwing exceptions at bad input (as we are already using try block)
    cin.exceptions(std::ios_base::failbit);
try {
    // start counting time
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    // Available command line arguments
    TCLAP::CmdLine cmdparser("Water order analysis", ' ', "1.0");

    TCLAP::ValueArg<string> trj_input_arg("f", "dcd", "Name of the trajectory file", true, "NULL", "string", cmdparser);
    TCLAP::ValueArg<string> atinfo_input_arg("s", "psf", "Name of the file containing the topology", true, "NULL", "string", cmdparser);
    TCLAP::ValueArg<size_t> start_frame_num_arg("", "start", "Frame number to start calculation from; default is first frame (counting starts from 0)", false, 0, "positive integer", cmdparser);
    TCLAP::ValueArg<size_t> end_frame_num_arg("", "stop", "Frame number to end calculation at; default is last frame (counting starts from 0)", false, 0, "positive integer", cmdparser);
    TCLAP::ValueArg<string> o_name_input_arg("c", "oxygen-name", "Name of oxgyen atoms in the atom-info file; default is OH2", false, "OH2", "string", cmdparser);
    TCLAP::ValueArg<string> out_file_input_arg("o", "output-file", "Base name of output file; default is \'Water_order\'", false, "Water_order", "string", cmdparser);
    TCLAP::ValueArg<double> bin_size_arg("", "bin-width", "Size of each range considered for averaging; default is 0.5", false, 0.5, "float (Angstrom)", cmdparser);
    TCLAP::ValueArg<double> rmax_arg("","rmax","Maximum distance from center-of-mass considered for distribution; default is 50.0",false, 50.0,"float (Angstrom)",cmdparser);
    TCLAP::SwitchArg no_rhov_arg("", "norhov", "If set, rhoV will not be calculated", cmdparser, false);
    TCLAP::SwitchArg no_com_arg("", "nocom", "If set, the center of mass will not be calculated. The droplet will be assumed to be pre-centered", cmdparser, false);
    TCLAP::ValueArg<int> omp_threads_arg("p", "pp", "Set number of OpenMPI threads; default is to use all available threads", false, omp_get_max_threads(), "", cmdparser);
    cmdparser.parse(argc, argv);

    // Parse command line options
    
    string atinfo_file_name = atinfo_input_arg.getValue();
    cout << "Topology file: \t\t\t" << atinfo_file_name << "\n";

    string trj_file_name = trj_input_arg.getValue();
    cout << "Trajectory file: \t\t" << trj_file_name << "\n";

    double rmax = rmax_arg.getValue();
    // For Voronoi box
    double box_max = rmax; 
    double box_min = rmax * -1.0; 
    int block_size = int(box_max*2.0/5.3); // ~5.3 Angstrom box size, from heuristic
    
    cout << "Using Rmax: \t\t\t" << rmax << " Angstrom\n" ;

    double bin_size = bin_size_arg.getValue();
    cout << "Using bin size: \t\t" << bin_size << " Angstrom\n" ;
    
    string sel_string = o_name_input_arg.getValue();
    cout << "Name of oxygen atom: \t\t" << sel_string << "\n";
    
    string out_file_name = out_file_input_arg.getValue();
    cout << "Output file name: \t\t" << out_file_name << "\n";

    omp_set_num_threads(omp_threads_arg.getValue());
    /* In this part, we read in the pdb or gro (or anything else) file that will
    give the names of atoms. This is crucial to detect the oxygen atoms */

    cout << "\n";
    cout << "Attempting to read the atom information file..."; // replace chemfiles with own code
    
    chemfiles::Trajectory atinfo(atinfo_file_name);
    chemfiles::Frame atinfo_frame = atinfo.read();
    if (atinfo_frame[0].name().empty()) {
        cout << "\n" << "Unable to read atom names from atom information file!" << "\n";
        cout << "File does not contain atom names, or is an unsupported format" << "\n" << "\n";
        cout << "Exiting" << "\n";
        exit(1);
    }
    else {
        cout << "  done" << "\n";
    }

    // now get the atoms selected by sel_string and handle any errors
    cout << "Parsing selection string...";

    string sel_string_fin = "name " + sel_string;
    auto select_oxy = chemfiles::Selection(sel_string_fin);
    std::vector<size_t> oxy_matches = select_oxy.list(atinfo_frame); // vector contains indices of oxgyen atoms
    const auto oxy_size = oxy_matches.size(); // total number of oxygen atoms found

    // if there are less than 4 O's then exit
    if (oxy_size < 5) {
        cout << "\n" << "There are 4 or less oxygen atoms (" << sel_string << ") in the system" << "\n";
        cout << "Order parameters can only be determined for >=5 oxygen atoms" << "\n";
        cout << "Exiting";
        exit(1);
    }
    else {
        cout << "  done" << "\n";
    }

     // Now read trajectory file
     cout << "Attempting to read the trajectory file..." ;
     chemfiles::Trajectory trj(trj_file_name);
    if (trj.nsteps() <= 1) {
        cout << "\n" << "There is 1 or less frames in the trajectory file given!\n";
        cout << "Exiting\n";
        exit(1);
    }
    else {
        cout << " done\n";
    }

    // check if the number of atoms is the same in atom info and trajectory files
    cout << "Checking compatibility of atom information and trajectory files...";
    // Testframe here!!
    trj.set_cell(chemfiles::UnitCell()); // ChemFiles has a problem with dcd files, set unit cell to no unit cell
    auto testframe = trj.read(); // for testing only!!
    if (testframe.size() == atinfo_frame.size()) {
        cout << "  done" << "\n";
    }
    else {
        cout << "\n" << "Number of atoms in the atom information file (" << atinfo_frame.size();
        cout << ") does not match the number of atoms in trajectory (" << testframe.size() << ") !\n";
        cout << "Please check the input files again.\n";
        cout << "Exiting" << std::endl;
        exit(1);
    }
    // now deal with the start and end frame
    size_t end_step;
    const size_t start_step = start_frame_num_arg.getValue();
    const size_t num_steps = trj.nsteps();

    if (start_frame_num_arg.isSet()) {
        if (start_step >= num_steps) {
            cout << "The value for first frame to be processed (" << start_step;
            cout << ") is larger than or equal to the total number of frames in the trajectory (" << num_steps << ").\n";
            cout << "Exiting" << std::endl;
            exit(1);
        }
    }
    if (end_frame_num_arg.isSet()) {
        if (end_frame_num_arg.getValue() > num_steps) {
            cout << "The value for last frame to be processed (" << end_frame_num_arg.getValue();
            cout << ") is larger than the total number of frames in the trajectory (" << num_steps << ").\n";
            cout << "Exiting" << std::endl;
            exit(1);
        }
        else {
            end_step = end_frame_num_arg.getValue();
        }
    }
    else {
        end_step = num_steps;
    }
    const auto num_processed_frames = end_step - start_step;
    cout << "Calculation will start from frame " << start_step << " and will end at frame " << (end_step-1) << " (Total " << num_processed_frames << " frames)" << "\n";
    
    // make a list of masses
    // 
    auto natoms = atinfo_frame.size();
    std::vector<double> mass_list(natoms);
    for (size_t k = 0; k < natoms; ++k) {
        mass_list.at(k) = atinfo_frame[k].mass();
    }
    const double total_mass = std::accumulate(mass_list.begin(),mass_list.end(),0.0);
    // check if the file type actually gives mass. If it doesn't then zero mass would be set
    if (std::fabs(total_mass - 0.0) < 0.1) {
        cout << "The atom information / topology file does not contain masses of atoms!\n";
        cout << "Exiting";
        exit(1);
    }

    // set up histogramming

    const double hist_start = 0.0; // set rmin = 0 from COM
    if (rmax <= 0.0) {
        cout << "Rmax value given (" << rmax << ") is less than or equal to zero\n";
        cout << "Exiting";
        exit(1);
    }
    if (bin_size <= 0.0) {
        cout << "Error! bin_size given (" << bin_size << ") is less than or equal to zero\n";
        cout << "Exiting";
        exit(1);
    }
    size_t num_bins = (size_t)round((rmax - hist_start) / bin_size);

    std::vector<double> bin_edges(num_bins + 1);
    // calculate bin edges
    for (int x = 0; x <= num_bins; ++x) {
        bin_edges.at(x) = hist_start + x * bin_size;
    }

    // Setup ofstream for order parameters

    string out_file_name_ord = out_file_name + "_ord.csv";
    //check if the output file exists, if not then create it and open it
    if (file_exists(out_file_name_ord)) {
        // file exists, probably from previous run ... exit showing an error message
        cout << "Output file named " << out_file_name_ord << " exists!" << "\n";
        cout << "Past output files will not be overwritten." << "\n";
        cout << "Exiting" << std::endl;
        exit(1);
    }
    std::ofstream outfile_ord;
    outfile_ord.open(out_file_name_ord.c_str());
    if (!outfile_ord) {
        cout << "File could not be opened!" << "\n";
        cout << "Exiting" << std::endl;
        exit(1);
    }
    outfile_ord << std::fixed << std::showpoint; // fixed form and always output with decimal point
    outfile_ord << std::setprecision(6); // set 6 decimal points for float output
    // print the header into file
    outfile_ord << "r,qT,d5,Sk\n";

    // Setup ofstream for densities

    string out_file_name_rho = out_file_name + "_rho.csv";
    //check if the output file exists, if not then create it and open it
    if (file_exists(out_file_name_rho)) {
        // file exists, probably from previous run ... exit showing an error message
        cout << "Output file named " << out_file_name_rho << " exists!" << "\n";
        cout << "Past output files will not be overwritten." << "\n";
        cout << "Exiting" << std::endl;
        exit(1);
    }
    std::ofstream outfile_rho; // this is the output stream for the d5 output file
    outfile_rho.open(out_file_name_rho.c_str());
    if (!outfile_rho) {
        cout << "File could not be opened!" << "\n";
        cout << "Exiting" << std::endl;
    }
    outfile_rho << std::fixed << std::showpoint; // fixed form and always output with decimal point
    outfile_rho << std::setprecision(6); // set 6 decimal points for float output
    // print the header into file
    if (no_rhov_arg == 0) {
    outfile_rho << "r,N,rho,rhoV\n";
    }
    else {
    outfile_rho << "r,N,rho\n";
    }
    
    // find all the heavy atoms, not just water for the Voronoi density
    std::vector<size_t> heavy_atom_matches;
    for (size_t k = 0; k < natoms; ++k) {
        if (mass_list.at(k) >= 3.0) { // heavy atom => anything heavier than hydrogen i.e. tritium?
            heavy_atom_matches.push_back(k);
        }
    }
    const auto n_heavy_atoms = heavy_atom_matches.size();
    if (n_heavy_atoms < oxy_size) {
        cout << "Number of heavy atoms is less that number of oxygen atoms.\n";
        cout << "Possibly an error in the masses in the topology.\n";
        cout << "Exiting" << std::endl;
        exit(1);
    }

    // for floating point overflow/underflow check
    std::feclearexcept(FE_UNDERFLOW);
    std::feclearexcept(FE_OVERFLOW);
    
    // input checks done, now onto main calculations

    std::vector<size_t> hist_counts(num_bins, 0); // histogram counts
    std::vector<double> hist_collect_q_tet(num_bins, 0.0); // collects the q_tet values  
    std::vector<double> hist_collect_d5(num_bins,0.0); // collects the d5 values      
    std::vector<double> hist_collect_sk(num_bins,0.0); // collects the Sk values
    
    std::vector<size_t> hist_counts_rhov(num_bins, 0); // histogram counts for rhov
    std::vector<double> hist_collect_rhov(num_bins); // collects the rho_V values

    // iterate over whole trajectory
    #pragma omp parallel
    {
    if (omp_get_thread_num() == 0) {
        cout << "\nRunning on " << omp_get_num_threads() << " thread(s)\n\n";
    }
    std::vector<double> dist_oo(oxy_size); // vector for holding O-O distances
    std::array<size_t, 4> nearest_four_ind = { 0,0,0,0 }; //stores 4 nearest indices
    auto frame = chemfiles::Frame(); // holds the frame, necessary to have a dummy variable due to scoping

    std::array<double, 3> ord_parm; // holds the order parameter values
    std::vector<size_t> hist_counts_priv(num_bins, (size_t)0);
    std::vector<double> hist_collect_q_tet_priv(num_bins, 0.0);
    std::vector<double> hist_collect_d5_priv(num_bins, 0.0);
    std::vector<double> hist_collect_sk_priv(num_bins,0.0);

    std::vector<double> rhov_val(n_heavy_atoms); // holds the rho_V value for all heavy atoms
    double x, y, z; // required to hold the coordinates
    // double x_max, y_max, z_max, x_min, y_min, z_min; // the max and min coordinates to get the box
    std::vector<size_t> hist_counts_rhov_priv(num_bins, 0); // thread private variable to hold data
    std::vector<double> hist_collect_rhov_priv(num_bins); // thread private variable
    
    #pragma omp for schedule(static, 1)
    for (openmp_index_type n = start_step; n < end_step; ++n) {
        #pragma omp critical
        {
        frame = trj.read_step(n);
        }
        auto positions_this_frame = frame.positions();
        auto com_of_frame = get_center_of_mass(frame, mass_list, natoms, total_mass, no_com_arg);

        for (size_t i = 0; i < oxy_size; ++i) {
            for (size_t j = 0; j < oxy_size; ++j) {
                dist_oo[j] = (positions_this_frame[oxy_matches[i]] - positions_this_frame[oxy_matches[j]]).norm();
            }

            auto com_o_dist = ((positions_this_frame[oxy_matches[i]]) - com_of_frame).norm(); // get distance of O from COM
            ord_parm = get_ord_parm(frame, dist_oo, oxy_size, i, oxy_matches);

            for (int x = 0; x < num_bins; ++x) {
                if (is_within_bounds(com_o_dist, bin_edges[x], bin_edges[x + 1])) {
                    hist_counts_priv[x]++;
                    hist_collect_q_tet_priv[x] += ord_parm[0];
                    hist_collect_d5_priv[x] += ord_parm[1];
                    hist_collect_sk_priv[x] += ord_parm[2];
                    break;
                }
            }              
        }
        if (no_rhov_arg == 0) {
            // a box big enough to store all the particles
            voro::container mybox(box_min, box_max, box_min, box_max, box_min, box_max, block_size, block_size, block_size, false, false, false, 1);

            for (size_t k = 0; k < n_heavy_atoms; ++k) {
                x = positions_this_frame[heavy_atom_matches[k]][0]-com_of_frame[0];
                y = positions_this_frame[heavy_atom_matches[k]][1]-com_of_frame[1];
                z = positions_this_frame[heavy_atom_matches[k]][2]-com_of_frame[2];
                mybox.put(k, x, y, z);
            }
            voro::c_loop_all loop_cursor(mybox);
            voro::voronoicell_neighbor this_cell;
            std::vector<int> neighbour_list;
            if (loop_cursor.start()) do if (mybox.compute_cell(this_cell, loop_cursor)) {
                auto k = loop_cursor.pid();
                this_cell.neighbors(neighbour_list);
                if (is_cell_at_edge(neighbour_list)) {
                    rhov_val.at(k) = 0.0;
                }
                else {
                    rhov_val.at(k) = num_to_dens * 1.0 / this_cell.volume(); // volume is in number per Angstrom^3
                }
            } while (loop_cursor.inc());
            // now deal with not all heavy atoms being oxygens
            for (size_t k = 0; k < n_heavy_atoms; ++k) {
                // find if the index in heavy atom match is also in oxygen match => it is an oxygen
                if (std::find(oxy_matches.begin(), oxy_matches.end(), heavy_atom_matches[k]) != oxy_matches.end()) {
                    auto com_o_dist = (positions_this_frame[heavy_atom_matches[k]] - com_of_frame).norm(); // get distance of O from COM
                    // populate the histogram
                    for (int q = 0; q < num_bins; ++q) {
                        if (is_within_bounds(com_o_dist, bin_edges[q], bin_edges[q + 1])) {
                            hist_counts_rhov_priv[q]++;
                            hist_collect_rhov_priv[q] += rhov_val[k];
                            break;
                        }
                    }       
                }
            }
        }

        if (n % 200 == 0) {
            cout << "\rFrame " << n << " processed";
            //for (float count = 0.0f; count <= 1.0f; count += 0.01f) {
            //    if (n == (int)(num_processed_frames * count)) cout << (int)(count * 100) << "% done...";
            //}
        }
    }
    // sum up the histograms
    #pragma omp critical
    for (int x = 0; x < num_bins; ++x) {
        hist_counts[x] += hist_counts_priv[x];
        hist_collect_q_tet[x] += hist_collect_q_tet_priv[x];
        hist_collect_d5[x] += hist_collect_d5_priv[x];
        hist_collect_sk[x] += hist_collect_sk_priv[x];
    }

    for (int q = 0; q < num_bins; ++q) { // x is reserved for coordinates
        hist_counts_rhov[q] += hist_counts_rhov_priv[q];
        hist_collect_rhov[q] += hist_collect_rhov_priv[q];
    }
    }
    // parallel region processing ends here

    // check if there were overflow/underflow
    if (std::fetestexcept(FE_UNDERFLOW) || std::fetestexcept(FE_OVERFLOW)) {
        cout << "Floating point error!\nExiting";
        exit(1);
    }

    // write the histogram x and y into the file
    double q_tet_intens = 0.0;
    double d5_intens = 0.0;
    double sk_intens = 0.0;
    for (int x = 0; x < num_bins; ++x) {
        if (hist_counts[x] == 0) {
            //y_intens = 0.0;
            outfile_ord << (bin_edges[x] + bin_edges[x + 1]) / 2 << ", , ," << "\n";
        }
        else {
            q_tet_intens = hist_collect_q_tet[x] / hist_counts[x];
            d5_intens = hist_collect_d5[x] / hist_counts[x];
            sk_intens = hist_collect_sk[x] / hist_counts[x];
            outfile_ord << (bin_edges[x] + bin_edges[x + 1]) / 2 << "," << q_tet_intens << "," << d5_intens << "," << sk_intens << "\n";
        }
    }
    outfile_ord << std::endl; //flush the output stream
    outfile_ord.close();
    cout << "\nOrder parameter calculation finished successfully!\n";
    cout << "Output written to " << out_file_name_ord << "\n";

    double n_intens = 0.0;
    double rho_intens = 0.0;
    double rhov_intens = 0.0;
    for (int x = 0; x < num_bins; ++x) {
        if (hist_counts[x] == 0) {
            //y_intens = 0.0;
            outfile_rho << (bin_edges[x] + bin_edges[x + 1]) / 2 << ", , , " << "\n";
        }
        else {
            n_intens = 1.0 * hist_counts[x] / num_processed_frames; // hist_counts here since hist_coounts_rhov includes heavy atoms
            double volele = four_pi_over_three * ((bin_edges[x + 1] * bin_edges[x + 1] * bin_edges[x + 1]) - (bin_edges[x] * bin_edges[x] * bin_edges[x]));
            rho_intens = n_intens / volele * num_to_dens;

            if (no_rhov_arg == 0) {
            rhov_intens = hist_collect_rhov[x] / hist_counts_rhov[x];
            outfile_rho << (bin_edges[x] + bin_edges[x + 1]) / 2 << "," << n_intens << "," << rho_intens << "," << rhov_intens << "\n";
            }

            else {
            outfile_rho << (bin_edges[x] + bin_edges[x + 1]) / 2 << "," << n_intens << "," << rho_intens << "\n";
            }
        }
    }
    outfile_rho << std::endl; //flush the output stream
    outfile_rho.close();
    cout << "\nDensities calculation finished successfully!\n";
    cout << "Output written to " << out_file_name_rho << "\n\n";

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    cout << std::setprecision(4);
    cout << "Total wall time elapsed (sec) = " << (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count())/1000000.0 << std::endl;
    // execution finished!!

} catch (const chemfiles::Error &chemex) {
    cout << "\n" << "Error in chemfiles:" << chemex.what() << std::endl;
    exit(1);
} catch (TCLAP::ArgException &tclex) {
    cout << "\n" << "Error in TCLAP:" << tclex.error() << "for arg " << tclex.argId() << std::endl;
    exit(1);
} catch (std::ios_base::failure &ioex) {
    cout << "\n" << "Error in keyboard input:" << ioex.what() << "\n";
    cout << "Please check that input data is of correct type!"<< std::endl;
    exit(1);
} catch (...) {
    cout << "\n" << "Other exception caught" << std::endl;
    exit(1);
}
    return 0;
}

/// <summary>

std::array<double, 3> get_ord_parm(const chemfiles::Frame& inputframe, const std::vector<double>& dist_list, size_t dist_list_size, size_t center, const std::vector<size_t>& oxy_ind_list) {
    std::array<double, 3> ord_parm = { 0,0,0 }; // These hold the three order parameters
    std::array<size_t, 5> near_ind = { 0,0,0,0,0 };
    std::array<double, 5> min = { -1.0,-1.0,-1.0,-1.0,-1.0 };

    // get ind1, min1
    for (size_t m = 0; m < dist_list_size; ++m) {
        if (m != center) {
            if (min[0] == -1.0 || dist_list[m] < min[0]) {
                min[0] = dist_list[m];
                near_ind[0] = m;
            }
        }
    }

    // get ind2, min2
    for (size_t m = 0; m < dist_list_size; ++m) {
        if (m != center && m != near_ind[0]) {
            if (min[1] == -1.0 || dist_list[m] < min[1]) {
                min[1] = dist_list[m];
                near_ind[1] = m;
            }
        }
    }

    // get ind3, min3
    for (size_t m = 0; m < dist_list_size; ++m) {
        if (m != center && m != near_ind[0] && m != near_ind[1]) {
            if (min[2] == -1.0 || dist_list[m] < min[2]) {
                min[2] = dist_list[m];
                near_ind[2] = m;
            }
        }
    }

    // get ind4, min4
    for (size_t m = 0; m < dist_list_size; ++m) {
        if (m != center && m != near_ind[0] && m != near_ind[1] && m != near_ind[2]) {
            if (min[3] == -1.0 || dist_list[m] < min[3]) {
                min[3] = dist_list[m];
                near_ind[3] = m;
            }
        }
    }

    // get ind5, min5
    for (size_t m = 0; m < dist_list_size; ++m) {
        if (m != center && m != near_ind[0] && m != near_ind[1] && m != near_ind[2] && m != near_ind[3]) {
            if (min[4] == -1.0 || dist_list[m] < min[4]) {
                min[4] = dist_list[m];
                near_ind[4] = m;
            }
        }
    }

    // Sk
    double sk_collect = 0.0;
    auto r_avg = (min[0] + min[1] + min[2] + min[3]) / 4;
    for (short int z = 0; z < 4; ++z) {
        sk_collect += std::pow((min[z] - r_avg), 2); // calculate sum
    }
    sk_collect = sk_collect / (4 * std::pow(r_avg, 2));

    // Q_tet
    double q_tet_collect = 0.0;
    for (short int j = 0; j < 3; ++j) {
        for (short int k = j + 1; k < 4; ++k) {
            q_tet_collect += std::pow(std::cos(inputframe.angle(oxy_ind_list[near_ind[j]], oxy_ind_list[center], oxy_ind_list[near_ind[k]])) + one_over_three, 2);
        }
    }

    ord_parm[0] = (1.0 - (three_over_eight * q_tet_collect));
    ord_parm[1] = min[4];
    ord_parm[2] = (1.0 - (one_over_three * sk_collect));
    
    return ord_parm; //d5,sk,qtet
}

/* This function evaluates the center of mass of a frame obtained from ChemFiles
    \param inputframe - reference to the ChemFiles frame object
    \param mass_list - a vector containing the masses of all atoms, from topology
    \param natoms - total number of atoms in the frame
    \param mass_total - total mass of the atoms in the frame */
chemfiles::Vector3D get_center_of_mass(const chemfiles::Frame& inputframe, const std::vector<double>& mass_list, const size_t natoms, const double mass_total, const bool no_com_arg)
{
    auto frame_com = chemfiles::Vector3D();
    if (no_com_arg == 1) {
        return frame_com;
    }
    auto atom_positions = inputframe.positions();
    for (size_t i = 0; i < natoms; ++i) {
        frame_com += atom_positions[i] * mass_list[i];
    }
    frame_com = frame_com / mass_total;
    return frame_com;
}

/// <summary>
/// This function checks if a file exists or not, by attempting to read the file with 
/// std::ifstream. If the file exists, return true.
/// </summary>
/// <param name="str">String containing the file name</param>
/// <returns>(bool) true if file exists</returns>
bool file_exists(const string& str)
{
    std::ifstream fs(str.c_str());
    return fs.is_open();
}

/* This function evaluates if a number is within a given range [low,high)
    \param value - the specified number
    \param low - lower end of range
    \param high - higher end of range */
bool is_within_bounds(const double value, const double low, const double high)
{
    return !(value < low) && (value < high);
}
/* This function evaluates if the cell is at the edge of the container box. If it at the edge, one of the neighbouring 
 particles would have ID of -1 to -6 (to indicate the 6 walls)
    \param neighbour_list - A std::vector<int> containing information about the neighbouring particles */
bool is_cell_at_edge(const std::vector<int>& neighbour_list)
{
    if (neighbour_list.size() == 0) {
        cout << "Neighbour list has 0 size, exiting.\n"<<std::endl;
        exit(1);
    }
    for (const auto z : neighbour_list) {
        if (z < 0) {
            return true;
        }
        else {
            return false;
        }
    }
    return false; // return false on unreachable codepath to silent compiler warning
}
