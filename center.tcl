set molname system
set trjname trajectory

###################################################################

mol new ${molname}.psf
mol addfile ${trjname}.dcd type dcd waitfor all

set nFrames [molinfo top get numframes]

# Modify this atomselection to include to starting point of the search 
set solute [atomselect top "protein or ion or name FLU or name IOD or name CES"]


animate goto start
set all [atomselect top all]

for {set i 0} {$i < $nFrames} {incr i} {
	set init [atomselect top "same residue as within 3 of (index $solute)"]
	set old [$init list]
	$init delete
	unset init

	set sel [atomselect top "same residue as within 3 of (index $old)"]
	set new [$sel list]
	$sel delete
	unset sel

	while {[llength $new] > [llength $old]} {
		set old $new
		set sel [atomselect top "same residue as within 3 of (index $old)"] 
		set new [$sel list]

		$sel delete
		unset sel		
	}

	set sel [atomselect top "same residue as within 3 of (index $old)"] 
	set cen [measure center $sel weight mass]

	$all moveby [vecscale -1 $cen]
	animate goto [expr $i+1]
}

animate goto 0
animate write dcd ${trjname}_centered.dcd end -1 waitfor all
